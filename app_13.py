import requests
from conf import *
from datetime import datetime

paragraph_count=int(input("write the number of paragraphs, more than 4: ") or 5)
if paragraph_count < 5:
    print("Must be more than 4")
    exit(12)

#print(DST_URL)

def get_content(DST_URL, paragraph_count):
    data = requests.get(DST_URL.format(paragraph_count))
    return data.json()

data_list = get_content(DST_URL, paragraph_count)
data_list_reverse = data_list.copy()
data_list_reverse.reverse()

i = 0
for paragraph in data_list:
    if "Pancetta" in paragraph:
        i =+ 1

text = ""
for paragraph in data_list_reverse:
    text = text + "\n" + paragraph


def file_write(file_path, info):
    file = open(file_path, "w")
    file.write(info)
    file.close()

datenow = datetime.now().strftime("%d.%m.%Y %H:%M")

info = ("Name: Rozhenko Andrii \n"
"Date: {} \n"
"Number of paragraphs with word Pancetta: {} \n"
"Text: {}").format(datenow, i, text)

file_write(file_path, info)

print("\nNow look in .\output.txt \n")
